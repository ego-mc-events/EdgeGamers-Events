# EdgeGamers-Events
Made with [Command Compiler Unlimited](https://github.com/Aquafina-water-bottle/Command-Compiler-Unlimited)

This is a collection of all CCU code that powers the minigames and events on the EdgeGamers Minecraft Event Server.

## Cloning instructions
Due to the inclusion of a git submodule (i.e. pictionary), instructions to clone this project have slightly changed.

To clone this repository, issue the following command in your terminal or command prompt
```
git clone --recurse-submodules https://gitlab.com/ego-mc-events/EdgeGamers-Events.git
```
Or alternatively, run the following commands in your terminal
```
git clone https://gitlab.com/ego-mc-events/EdgeGamers-Events.git
cd EdgeGamers-Events
git submodule init
git submodule update
```
Click [here](https://git-scm.com/book/en/v2/Git-Tools-Submodules) to learn more about [submodules](https://git-scm.com/docs/git-submodule)

## Updating submodules
To update any submodules in this project (i.e. pictionary), simply run `git submodule update --remote --merge`  
OR, simply cd into the submodule folder (i.e. functions/ego/pictionary) and run `git pull origin master`

Finally, commit your changes.
