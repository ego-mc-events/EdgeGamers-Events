scoreboard players set @a gSA 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLtp 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLpvp 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLgmd 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLwea 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLreg 0
gamerule naturalRegeneration true
scoreboard players set @e[type=armor_stand,tag=FlooStand,score_FLgam_min=1368311757,score_FLgam=1368311757] FLgam 0
tellraw @a[score_EC_min=0,score_EC=0] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"ST","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Sand Tomb","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1368311757"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Sand Tomb","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Sand Tomb","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1368311757"}},{"text":" has stopped!","color":"red"}]}
execute @a[m=2,score_STpl_min=1] ~ ~ ~ function ego:sand_tomb/full_reset_player
scoreboard objectives remove ST
scoreboard objectives remove STti
scoreboard objectives remove STst
scoreboard objectives remove STpl
scoreboard objectives remove STpt
scoreboard objectives remove STid
scoreboard objectives remove STwc
scoreboard objectives remove STcl
scoreboard teams remove ST
scoreboard teams remove STd_dg
scoreboard teams remove STd_g
scoreboard teams remove STd_y
fill -222 4 12 -202 44 25 air 0 replace sand 0
fill -272 4 35 -228 49 41 air 0 replace sand 0
kill @e[x=-279,y=4,z=5,dx=83,dy=50,dz=44,type=falling_block]
kill @e[tag=STEntity]
