function ego:floo_network/stop_events
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLtp 1368311757
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLpvp 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLwea 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLreg 0
scoreboard players set @a gSA 0
scoreboard players set @a[x=-279,y=4,z=5,dx=83,dy=50,dz=44] gSA 1
scoreboard objectives add constants dummy
scoreboard players set -1 constants -1
scoreboard players set 20 constants 20
scoreboard objectives add ST dummy Sand Tomb
scoreboard objectives setdisplay sidebar ST
scoreboard objectives add STti dummy Sand Tomb Timer
scoreboard objectives add STst dummy Sand Tomb State
scoreboard objectives add STpl dummy Sand Tomb Player List
scoreboard objectives add STpt dummy Sand Tomb Portal
scoreboard objectives add STid dummy Sand Tomb Arena ID
scoreboard objectives add STwc dummy Sand Tomb Win Counter
scoreboard objectives add STcl dummy Sand Tomb Calculations
scoreboard teams add ST Sand Tomb
scoreboard teams option ST color yellow
scoreboard teams option ST friendlyfire false
scoreboard teams option ST collisionRule never
scoreboard teams add STd_dg Sand Tomb display dark green
scoreboard teams option STd_dg color dark_green
scoreboard teams add STd_g Sand Tomb display green
scoreboard teams option STd_g color green
scoreboard teams add STd_y Sand Tomb display yellow
scoreboard teams option STd_y color yellow
scoreboard teams join STd_dg Classic Ant_Farm
scoreboard teams join STd_y Players Time_Elapsed
summon armor_stand ~ ~ ~ {Tags:["STEntity","STStand"],Invulnerable:1,NoGravity:1,Invisible:1,Marker:1b}
scoreboard players set @e[type=armor_stand,tag=STStand] STst 0
scoreboard players set @e[type=armor_stand,tag=STStand] STid 1
fill -210 4 60 -210 4 60 redstone_block
fill -210 4 60 -210 4 60 stonebrick
fill -220 13 58 -220 13 57 redstone_block
fill -220 13 58 -220 13 57 stonebrick
scoreboard players set &Delay STcl 60
tellraw @a[score_EC_min=0,score_EC=0] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"ST","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Sand Tomb","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1368311757"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Sand Tomb","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Sand Tomb","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1368311757"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=armor_stand,tag=FlooStand,score_FLgam_min=0,score_FLgam=0] FLgam 1368311757
