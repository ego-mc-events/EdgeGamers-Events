function ego:floo_network/stop_events
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLpvp 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLwea 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLreg 0
scoreboard players set @a gSA 0
scoreboard players set @a[x=544,y=19,z=37,dx=10,dy=5,dz=10] gSA 1
scoreboard objectives add constants dummy
scoreboard players set -1 constants -1
scoreboard objectives add TS dummy Teamspeak Token
scoreboard objectives setdisplay sidebar TS
scoreboard objectives add TSst dummy Teamspeak Token State
scoreboard objectives add TSpl dummy Teamspeak Token Player List
scoreboard objectives add TScn dummy Teamspeak Token Player Counter
scoreboard objectives add TSpw dummy Teamspeak Token Password storage
scoreboard objectives add TSdt dummy Teamspeak Token Display Type
scoreboard objectives add TSipw trigger Teamspeak Token Input password
summon armor_stand ~ ~ ~ {Tags:["TSEntity","TSStand"],Invulnerable:1,NoGravity:1,Invisible:1,Marker:1b}
scoreboard players set @e[type=armor_stand,tag=TSStand] TSdt 0
scoreboard players set @e[type=armor_stand,tag=TSStand] TSst 0
fill 547 5 102 535 5 102 redstone_block 0 replace stonebrick 0
fill 547 5 102 535 5 102 stonebrick 0 replace redstone_block 0
function ego:ts_token/request_password
tellraw @a[score_EC_min=0,score_EC=0] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"TSToken","color":"dark_aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Teamspeak Token","color":"dark_aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 2052427056"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Teamspeak Token","color":"dark_aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Teamspeak Token","color":"dark_aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 2052427056"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=armor_stand,tag=FlooStand,score_FLgam_min=0,score_FLgam=0] FLgam 2052427056
