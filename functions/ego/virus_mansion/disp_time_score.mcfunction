scoreboard players operation Seconds VRMti = @s VRMti
scoreboard players operation Seconds VRMti /= 20 constants
scoreboard players operation Seconds VRMti %= 60 constants
scoreboard players add Seconds VRMti 1
scoreboard players operation Seconds VRM = Seconds VRMti
scoreboard players operation Minutes VRMti = @s VRMti
scoreboard players operation Minutes VRMti /= 1200 constants
scoreboard players operation Minutes VRM = Minutes VRMti
