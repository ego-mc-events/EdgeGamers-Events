scoreboard players set @a gSA 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLtp 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLpvp 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLgmd 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLwea 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLreg 0
gamerule naturalRegeneration true
scoreboard players set @e[type=armor_stand,tag=FlooStand,score_FLgam_min=1973922275,score_FLgam=1973922275] FLgam 0
tellraw @a[score_EC_min=0,score_EC=0] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRM","color":"gold","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Mansion Virus","color":"gold"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1973922275"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Mansion Virus","color":"gold","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Mansion Virus","color":"gold"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1973922275"}},{"text":" has stopped!","color":"red"}]}
execute @a[score_VRMpl_min=1,score_VRMpl=2,m=2] ~ ~ ~ function ego:virus_mansion/full_reset_player
scoreboard players reset * HOST
kill @e[type=armor_stand,tag=VRMEntity]
setblock -160 18 -1211 iron_trapdoor facing=west,half=top,open=false
playsound minecraft:block.iron_trapdoor.open voice @a -160 18 -1211
fill -152 11 -1213 -152 11 -1209 redstone_block 0 replace stonebrick 0
scoreboard objectives remove VRM
scoreboard objectives remove VRMpl
scoreboard objectives remove VRMsa
scoreboard objectives remove VRMti
scoreboard objectives remove VRMchi
scoreboard objectives remove VRMcvr
scoreboard objectives remove VRMgl
scoreboard objectives remove VRMcalc
scoreboard objectives remove VRMst
scoreboard teams remove VRMh
scoreboard teams remove VRMv
scoreboard teams remove VRMd_y
scoreboard teams remove VRMd_g
