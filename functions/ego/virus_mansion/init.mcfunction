function ego:floo_network/stop_events
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLtp 1973922275
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLpvp 2
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLwea 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLreg 0
scoreboard players set @a gSA 0
scoreboard players set @a[x=-46,y=78,z=-1170,dx=-121,dy=-65,dz=-88] gSA 1
scoreboard objectives add constants dummy
scoreboard players set 1200 constants 1200
scoreboard players set 20 constants 20
scoreboard players set 60 constants 60
scoreboard objectives add VRM dummy Mansion Virus
scoreboard objectives setdisplay sidebar VRM
scoreboard objectives add VRMpl dummy Mansion Virus Player List
scoreboard objectives add VRMsa dummy Mansion Virus Select All
scoreboard objectives add VRMti dummy Mansion Virus Timer
scoreboard objectives add VRMchi dummy Mansion Virus Count Hiders
scoreboard objectives add VRMcvr dummy Mansion Virus Count Virus
scoreboard objectives add VRMgl dummy Mansion Virus Glowing Players
scoreboard objectives add VRMcalc dummy Mansion Virus Calculations
scoreboard objectives add VRMst dummy Mansion Virus State
scoreboard teams add VRMh Mansion Virus Hiders
scoreboard teams option VRMh friendlyfire false
scoreboard teams option VRMh collisionRule never
scoreboard teams option VRMh deathMessageVisibility always
scoreboard teams option VRMh color green
scoreboard teams option VRMh nametagVisibility hideForOtherTeams
scoreboard teams add VRMv Mansion Virus Virus
scoreboard teams option VRMv friendlyfire false
scoreboard teams option VRMv collisionRule never
scoreboard teams option VRMv deathMessageVisibility always
scoreboard teams option VRMv color yellow
scoreboard teams add VRMd_y Mansion Virus Display Yellow
scoreboard teams option VRMd_y color yellow
scoreboard teams add VRMd_g Mansion Virus Display Green
scoreboard teams option VRMd_g color green
scoreboard players set @s HOST 0
scoreboard teams join VRMd_y Countdown Minutes Seconds Virus
scoreboard teams join VRMd_g Hiders
scoreboard players set &Countdown VRMcalc 1200
scoreboard players set &Glowing VRMcalc 6000
scoreboard players set &GameTime VRMcalc 12000
summon armor_stand ~ ~ ~ {Tags:["VRMStand","VRMEntity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
execute @e[type=armor_stand,tag=VRMStand] ~ ~ ~ function ego:virus_mansion/reset_round
tellraw @a[score_EC_min=0,score_EC=0] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRM","color":"gold","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Mansion Virus","color":"gold"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1973922275"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Mansion Virus","color":"gold","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Mansion Virus","color":"gold"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1973922275"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=armor_stand,tag=FlooStand,score_FLgam_min=0,score_FLgam=0] FLgam 1973922275
